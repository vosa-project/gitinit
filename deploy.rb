#!/usr/bin/ruby
require "net/http"
require "json"
require 'open3'

host = `dmidecode -s system-product-name`.strip
uuid = `dmidecode -s system-version`.strip

# Test data
# host = "https://i-dev.itcollege.ee"
# uuid = "911da568-9ab7-4a4c-8e03-190f0ac2ed2c"

root = "/root"
scripts_folder = "scripts"

# Fetch lab name and GIT address
begin
  uri = URI("#{host}/labinfo.json?uuid=#{uuid}")
  res = Net::HTTP.get_response(uri)
  json = JSON.parse(res.body)
  begin
    name = json["lab"]["name"]
  rescue NoMethodError
    abort("Could not retrieve lab name")
  end
  begin
    git_secured = false
    git = json["lab"]["config"]["git"]
  rescue NoMethodError
    abort("Could not retrieve lab GIT address")
  end
  begin
    #gitkey = json["lab"]["config"]["gitkey"]
    authtoken = json["lab"]["config"]["authtoken"]
    git_secured = true
  rescue NoMethodError
    puts "No access details provided with Git repository. Assuming that no authentication is needed."
  end
  rescue SocketError
  puts "Host is not reachable"
end

git_secured = true

# Currently Github support only

if git_secured
  # prefix = "https://"
  # suffix = "github.com"
  # username = git[/#{prefix}(.*?)#{suffix}/m, 1]
  git = git.gsub("https://github.com", "https://#{authtoken}@github.com")
  puts "Cloning repo with preissued token."
  puts "Cloning repo from #{git}"
end

# Clone the repo
cmd = "cd #{root} && git clone --progress #{git} #{scripts_folder}"
git_success = false

Open3.popen3(cmd) do |stdin, stdout, stderr, wait_thr|
  for line in stderr do
    #puts "- #{line}"
    if line.include? "remote: Total"
      git_success = true
    end 
    if line.include? "already exists"
      git_success = false
      abort("Lab is already loaded with content. You can refetch by restarting lab or 'systemctl reload labchecks'")
    end
  end
end

# Check if lab's branch is created
if git_success
  puts "Repo cloned. Seeking for branch named #{name}..."
  branch_found = false
  Open3.popen3("cd #{root}/#{scripts_folder} && git branch -a") do |stdin, stdout, stderr, wait_thr|
    for line in stdout do
      #puts "+ #{line}"
      if line.include? name
        branch_found = true
      end 
    end
  end
else
  abort("Could not clone git repo #{git}. Exiting. We're fucked.")
end

if branch_found
  # Branch found. Checking out
  puts "Found our branch. Checking out..."
  branch_switched = false
  Open3.popen3("cd #{root}/#{scripts_folder} && git checkout #{name}") do |stdin, stdout, stderr, wait_thr|
    for line in stderr do
      #puts "+ #{line}"
      if line.include? "Switched to a new branch '#{name}'"
        branch_switched = true
      end 
    end
  end

  if branch_switched
    puts "Setting branch '#{name}' as upstream"
    upstream_switched = false
    Open3.popen3("cd #{root}/#{scripts_folder} && git branch --set-upstream-to origin/#{name}") do |stdin, stdout, stderr, wait_thr|
      for line in stdout do
        #puts "+ #{line}"
        if line.include? "track remote branch"
          upstream_switched = true
        end 
      end
    end
  end

  if upstream_switched
    puts "'#{name}' is now set up. Launching script services from setup.sh"
    Open3.popen3("cd #{root}/#{scripts_folder} && bash setup.sh") do |stdin, stdout, stderr, wait_thr|
      for line in stderr do
        puts "+ #{line}"
      end
    end
  else
    puts "Could not set '#{name}' as upstream. Don't really care. Launching script services from setup.sh"
    Open3.popen3("cd #{root}/#{scripts_folder} && bash setup.sh") do |stdin, stdout, stderr, wait_thr|
      for line in stderr do
        puts "+ #{line}"
      end
    end
  end
else
  # Creating new branch 
  puts "No #{name} branch found. Creating new branch named '#{name}'"
  branch_init = false
  Open3.popen3("cd #{root}/#{scripts_folder} && git checkout -b #{name} master") do |stdin, stdout, stderr, wait_thr|
    for line in stderr do
      #puts "+ #{line}"
      if line.include? "new branch"
        branch_init = true
      end
    end
  end

  if branch_init
    # Pushing new branch to repo
    puts "New branch inited. Pushing to the clouds"
    branch_completed = false
    Open3.popen3("cd #{root}/#{scripts_folder} && git push --set-upstream origin #{name}") do |stdin, stdout, stderr, wait_thr|
      for line in stderr do
        #puts "+ #{line}"
        if line.include? "-> #{name}"
          branch_completed = true
        end
      end
    end
  end

  if branch_completed
    puts "Branch pushed. Git init done. Launching setup.sh to create check services."
    Open3.popen3("cd #{root}/#{scripts_folder} && bash setup.sh") do |stdin, stdout, stderr, wait_thr|
      for line in stderr do
        puts "+ #{line}"
      end
    end
  else
    puts "Branch was not pushed."
  end
end
